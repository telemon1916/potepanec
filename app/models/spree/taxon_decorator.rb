Spree::Taxon.class_eval do
  def taxons_related_products
    self_and_descendants.inject([]) do |result, taxon|
      result + taxon.products
    end
  end

  def count_products_of_self_and_descendants
    self_and_descendants.inject(0) do |result, taxon|
      result + taxon.products.count
    end
  end
end

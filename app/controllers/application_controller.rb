class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  def render_404(exception = '')
    logger.info "Rendering 404 with exception: #{exception.message}" if exception
    render file: Rails.root.join('public/404.html'),
           status: 404,
           layout: 'application',
           content_type: 'text/html'
  end
end

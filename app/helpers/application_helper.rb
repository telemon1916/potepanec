module ApplicationHelper
  APP_NAME = 'BIGBAG Store'.freeze

  def page_title(title = '')
    base_title = APP_NAME
    return base_title if title.blank?
    "#{base_title} | #{title}"
  end

  def add_breadcrumb(name, path)
    @breadcrumbs ||= [OpenStruct.new(name: "HOME", path: potepan_root_path)]
    @breadcrumbs << OpenStruct.new(name: name, path: path) if @breadcrumbs
  end

  def breadcrumbs
    @breadcrumbs
  end
end

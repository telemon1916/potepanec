require 'rails_helper'

RSpec.describe Spree::Taxon, type: :model do
  let!(:taxon) { create(:taxon, name: 'Clothing') }
  let!(:child_taxon) { taxon.children.create(name: 'Shirt') }
  let!(:other_taxon) { create(:taxon) }
  let!(:products) do
    products = create_list(:product, 5)
    products.each { |product| product.taxons << child_taxon }
  end

  describe "taxons_related_products" do
    it "returns no product" do
      expect(other_taxon.taxons_related_products).to be_empty
    end

    it "returns products" do
      expect(child_taxon.taxons_related_products).to eq(products)
    end
  end

  describe "count_taxons_products" do
    it "returns zero" do
      expect(other_taxon.count_products_of_self_and_descendants).to eq(0)
    end

    it "returns number of taxons products" do
      expect(child_taxon.count_products_of_self_and_descendants).
        to eq(products.count)
    end
  end
end

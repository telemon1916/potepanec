require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do
  controller do
    def show
      raise ActiveRecord::RecordNotFound
    end
  end

  describe "handling ActiveRecord::RecordNotFound exceptions" do
    it "returns status 404" do
      get :show, params: { id: "bad_id" }
      expect(response).to have_http_status(404)
    end

    it "renders the public/404.html template" do
      get :show, params: { id: "bad_id" }
      expect(response).
        to render_template(file: Rails.root.join('public/404.html').to_s)
    end
  end
end

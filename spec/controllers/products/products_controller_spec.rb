require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let!(:product) { create(:product) }

    before do
      get :show, params: { id: id }
    end

    context "when the product exists" do
      context "id is given" do
        let(:id) { product.id }

        it "responds successfully and returns 200" do
          expect(response).to be_successful
          expect(response).to have_http_status(:success)
          expect(assigns(:product)).to eq(product)
          expect(response).to render_template(:show)
        end
      end

      context "slug is given" do
        let(:id) { product.friendly_id }

        it "responds successfully and returns 200" do
          expect(response).to be_successful
          expect(response).to have_http_status(:success)
          expect(assigns(:product)).to eq(product)
          expect(response).to render_template(:show)
        end
      end
    end

    context "when the product does not exist" do
      context "nonexistent id is given" do
        let(:id) { Spree::Product.last.id + 1 }

        it "returns 404 and renders 404.html" do
          expect(response).to have_http_status(:not_found)
          expect(assigns(:product)).to be_nil
          expect(response).
            to render_template(file: Rails.root.join('public/404.html').to_s)
        end
      end

      context "nonexistent slug id given" do
        let(:id) { 'unexist' }

        it "returns 404 and renders 404.html" do
          expect(response).to have_http_status(:not_found)
          expect(assigns(:product)).to be_nil
          expect(response).
            to render_template(file: Rails.root.join('public/404.html').to_s)
        end
      end
    end
  end
end

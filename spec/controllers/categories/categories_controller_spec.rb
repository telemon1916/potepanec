require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let!(:taxonomies) { create(:taxonomy) }
    let!(:taxon) { taxonomies.root.children.create(name: 'Ruby') }
    let!(:products) do
      products = create_list(:product, 5)
      products.each { |product| product.taxons << taxon }
    end

    before do
      get :show, params: { id: taxon_id }
    end

    context "when the taxon exist" do
      let(:taxon_id) { taxon.id }

      it "responds successfully and returns 200" do
        expect(response).to be_successful
        expect(response).to have_http_status(:success)
        expect(assigns(:taxon)).to eq(taxon)
        expect(assigns(:taxonomies)).to eq([taxonomies])
        expect(assigns(:products)).to eq(products)
        expect(response).to render_template(:show)
      end
    end

    context "when the taxon does not exist" do
      let(:taxon_id) { Spree::Taxon.last.id + 1 }

      it "returns 404 and renders 404.html" do
        expect(response).to have_http_status(:not_found)
        expect(assigns(:taxon)).to be_nil
        expect(assigns(:taxonomies)).to be_nil
        expect(assigns(:products)).to be_nil
        expect(response).
          to render_template(file: Rails.root.join('public/404.html').to_s)
      end
    end
  end
end

require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "page_title" do
    it "returns base_title only" do
      expect(helper.page_title).to eq('BIGBAG Store')
    end

    it "returns with provided title" do
      expect(helper.page_title('product page')).
        to eq('BIGBAG Store | product page')
    end
  end

  describe "breadcrumbs" do
    let!(:product) { create(:product) }

    it "returns nil" do
      expect(helper.breadcrumbs).to be nil
    end

    it "returns breadcrums" do
      breadcrumbs = [
        OpenStruct.new(name: "HOME", path: potepan_root_path),
        OpenStruct.new(name: product.name, path: potepan_product_path(product)),
      ]
      expect(helper.add_breadcrumb(product.name, potepan_product_path(product))).
        to match(breadcrumbs)
    end
  end
end

require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let!(:product) { create(:product) }

  scenario "access to products/show" do
    visit potepan_product_path(product)
    within('div.media-body.product') do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    within('div#footer') do
      expect(page).to have_content Time.zone.now.year
    end
  end
end

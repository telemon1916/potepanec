require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  include ApplicationHelper

  given!(:taxonomy) { create(:taxonomy, name: 'Categories') }
  given!(:taxon) { taxonomy.root.children.create(name: 'Clothing') }
  given!(:child_taxon) { taxon.children.create(name: 'T-Shirt') }
  given!(:product) do
    product = create(:product, name: 'Superman T-Shirt')
    product.taxons << child_taxon
  end

  feature "access to categrories/show" do
    background do
      visit potepan_category_path(child_taxon.id)
    end

    scenario "display the page title" do
      expect(page).to have_title child_taxon.name
    end

    scenario "display breadcrumbs" do
      breadcrumbs = [
        OpenStruct.new(name: 'HOME', path: potepan_root_path),
        OpenStruct.new(name: child_taxon.name, path: potepan_category_path(child_taxon)),
      ]

      within('div.page-title') do
        expect(page).to have_content breadcrumbs.last.name
      end

      within('ol.breadcrumb') do
        expect(page).to have_link 'HOME'
        within('li.active') do
          expect(page).to have_content child_taxon.name
        end
      end
    end

    scenario "display taxon's related products" do
      within('div.productBox') do
        expect(page).to have_content child_taxon.products.first.name
        expect(page).to have_content child_taxon.products.first.display_price
      end
    end
  end
end
